/**
 * Implement Gatsby's Browser APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/browser-apis/
 */

// You can delete this file if you're not using it
import ApolloClient from 'apollo-boost'

export const client = new ApolloClient({
  uri: 'http://localhost:8080/graphql',
})
