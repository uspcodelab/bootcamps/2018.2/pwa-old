import React, { Component } from 'react'

import '../styles/main.scss'
import Card from '../components/card'
import ListItem from '../components/listItem'
import Header from '../components/header'
import Typhlosion from '../images/typhlosion.gif'

export default class User extends Component {
  constructor(props) {
    super(props)
    if (!this.props.options) this.options = []
    else this.options = this.props.options
    this.options = [
      {
        name: 'Hackaillon 2017',
      },
      {
        name: 'HackXmas 2017',
      },
      {
        name: 'Hallowackathon 2017',
      },
    ]
  }

  render() {
    return (
      <div>
        <Card style="list">
          <div className="flex flex-row">
            <div className="w-30">
              <img src={Typhlosion} />
            </div>
            <div className="w-70 flex flex-column">
              <h2 className="primary-color">Typhlosion</h2>
              <p>
                Typhlosion obscures itself behind a shimmering heat haze that it
                creates using its intensely hot flames. This Pokémon creates
                blazing
              </p>
            </div>
          </div>
        </Card>
        <Header>Participações</Header>
        {this.options.map(el => (
          <ListItem key={el}>
            <img src={Typhlosion} style={{ width: '50px', height: '50px' }} />&nbsp;
            <span className="tc">{el.name}</span>&nbsp;
          </ListItem>
        ))}
      </div>
    )
  }
}
