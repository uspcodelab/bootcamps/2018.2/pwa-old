import React from 'react'
import Link from 'gatsby-link'
import '../styles/main.scss'
import Banner from '../components/banner'
import Navbar from '../components/navbar'
import EditionsList from '../components/editionsList'
import Dropdown from '../components/dropdown'

const IndexPage = () => (
  <div>
    <div className="bg-light-gray">
      <Navbar home={true} />
      <Banner
        title="A melhor plataforma de hackathons"
        subTitle="Organize e promova hackathons numa plataforma completamente integrada"
      />
    </div>
    <EditionsList />
  </div>
)

export default IndexPage
