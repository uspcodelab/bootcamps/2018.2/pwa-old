import React, { Component } from 'react'
import gql from 'graphql-tag'
import { client } from '../../gatsby-browser'

import FormSign from '../components/formSign'

const signup = gql`
  mutation funcSignUp($email: String!, $password: String!, $name: String!) {
    signup(email: $email, password: $password, name: $name) {
      token
    }
  }
`

class Signup extends Component {
  constructor(props) {
    super(props)

    this.signupHandler = this.signupHandler.bind(this)
  }

  async signupHandler({ email, password, name }) {
    await client.mutate({
      mutation: signup,
      variables: {
        email,
        password,
        name,
      },
    })
    this.props.history.push('/login')
  }

  render() {
    const state = {
      email: '',
      password: '',
      name: '',
      password_confirmation: '',
    }
    return <FormSign type="signup" state={state} handler={this.signupHandler} />
  }
}

export default Signup
