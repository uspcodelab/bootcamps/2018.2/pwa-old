import React from 'react'
import Link from 'gatsby-link'
import logo from '../images/logo.png'
import Button from '../components/button'

class Banner extends React.Component {
  render() {
    return (
      <div className="w-100 pb3">
        <div className="w-100 pa1 flex flex-column">
          {/* <img src={logo} className="mr-auto ml-auto h4 h5-ns" /> */}
          <h1 className="ma4-l mb0 tc f-headline-sm f-headline-ns title avenir title">
            {this.props.title}
          </h1>
        </div>
        <div className="w-100 pa1">
          <h2 className="ma1-l tc f4 code mid-gray">{this.props.subTitle}</h2>
        </div>
      </div>
    )
  }
}
export default Banner
