import React, { Component } from 'react'
import '../styles/main.scss'

export default class ListItem extends Component {
  constructor(props) {
    super(props)

    this.classes =
      'bg-near-white flex justify-between ph2 items-center pv2 f4 fw3 mid-gray bt bb b--light-gray'
  }

  render() {
    return <div className={this.classes}>{this.props.children}</div>
  }
}
