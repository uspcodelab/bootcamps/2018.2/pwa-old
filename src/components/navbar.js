import Link from 'gatsby-link'
import React, { Component } from 'react'
import IonIcon from 'react-ionicons'
import Aside from '../components/aside'
import logo from '../images/logo.png'

class Navbar extends Component {
  constructor(props) {
    super(props)

    this.state = {
      open: false,
    }
  }
  toggleAside() {
    this.setState({
      open: !this.state.open,
    })
  }
  render() {
    let color = ''
    if (!this.props.home) color = ' bg-light-gray'

    let Dimmed = null
    let hamburgerClass =
      'hamburger hamburger--collapse primary absolute right-0 z-3'
    if (this.state.open) {
      hamburgerClass += ' is-active white'
      Dimmed = (
        <div
          className="absolute top-0 bottom-0 left-0 right-0 bg-black o-30 overflow-hidden z-1"
          onClick={() => this.toggleAside()}
        />
      )
    }

    return (
      <div>
        <nav className={'flex justify-between pv2' + color}>
          <a href="/">
            <div
              className="logo mh2"
              style={{ backgroundImage: `url(${logo})` }}
            />
          </a>
          <a href="/" className="no-underline fw9 f3">
            <div className="flex items-center h-100 primary-color b">
              Hacknizer
            </div>
          </a>
          <a href="#">
            <button
              className={hamburgerClass}
              type="button"
              onClick={() => this.toggleAside()}
            >
              <span className="hamburger-box">
                <span className="hamburger-inner" />
              </span>
            </button>
          </a>
        </nav>
        {Dimmed}
        <Aside activated={this.state.open} />
      </div>
    )
  }
}

export default Navbar
