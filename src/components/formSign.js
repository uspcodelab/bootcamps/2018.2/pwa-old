import React, { Component } from 'react'
import Link from 'gatsby-link'
import '../styles/main.scss'
import TextField from '@material-ui/core/TextField'
import Button from '../components/button'
import logo from '../images/logo.png'

class FormSign extends Component {
  constructor(props) {
    super(props)

    this.state = this.props.state

    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  handleChange(event) {
    this.setState({ [`${event.target.name}`]: event.target.value })
  }

  renderTitle() {
    const title =
      this.props.type === 'signup'
        ? 'Cadastrar-se no Hacknizer'
        : 'Entrar no Hacknizer'

    return <h2 className="mb0 fw4"> {title} </h2>
  }

  renderSubtitle() {
    let question, route, action

    if (this.props.type === 'signup') {
      question = 'Já possui conta?'
      route = 'login'
      action = 'Entre aqui'
    } else {
      question = 'Novo por aqui?'
      route = 'signup'
      action = 'Criar conta'
    }

    return (
      <p className="mt0 fw1">
        <span> {question} </span>
        <Link to={route}>{action}</Link>
      </p>
    )
  }

  renderForm() {
    let name, password_confirmation

    const arr = [
      {
        label: 'Nome',
        type: 'text',
        name: 'name',
      },
      {
        label: 'Email',
        type: 'email',
        name: 'email',
      },
      {
        label: 'Senha',
        type: 'password',
        name: 'password',
      },
      {
        label: 'Confirme sua senha',
        type: 'password',
        name: 'password_confirmation',
      },
    ]

    const fields = arr.map(form => {
      let item = ''
      if (
        this.props.type !== 'signup' &&
        (form.name === 'name' || form.name === 'password_confirmation')
      )
        item = ''
      else
        item = (
          <TextField
            className="mb2"
            label={form.label}
            name={form.name}
            type={form.type}
            onChange={this.handleChange}
            fullWidth
            key={`${form.name}_${this.props.type}`}
          />
        )

      return item
    })

    return <div> {fields} </div>
  }

  renderButton() {
    let text

    if (this.props.type === 'signup') {
      text = 'Cadastre-se'
    } else {
      text = 'Entrar'
    }

    return (
      <div className="mt4 mb3">
        <Button onClick={this.handleSubmit} style="outline" text={text} />
      </div>
    )
  }

  renderLink() {
    if (this.props.type !== 'signup') return <a href="#">Esqueceu a senha?</a>
    return ''
  }

  handleSubmit() {
    let args = {
      email: this.state.email,
      password: this.state.password,
    }

    if (this.props.type === 'signup') {
      args.name = this.state.name
      args.password_confirmation = this.state.password_confirmation
    }

    console.log(args)

    this.props.handler(args)
  }

  render() {
    return (
      <div className="pb5 pt4 bg-near-white">
        <div className="flex justify-center flex-column tc">
          <div>
            <Link to="/">
              <img className="w4" src={require('../images/logo.png')} />
            </Link>
          </div>
          <div>
            {this.renderTitle()}
            {this.renderSubtitle()}
          </div>
        </div>
        <div className="mh4 pv4 ph3 flex justify-center flex-column tc br3 shadow-4 bg-white">
          <div>
            {this.renderForm()}
            {this.renderButton()}
            {this.renderLink()}
          </div>
        </div>
      </div>
    )
  }
}

export default FormSign
