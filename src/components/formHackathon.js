import React from 'react'
import Button from '../components/button'

class MetadataField extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      id: props.id,
      name: props.name,
      type: props.type,
    }

    this.handleChangeName = this.handleChangeName.bind(this)
    this.handleChangeType = this.handleChangeType.bind(this)
  }

  handleChangeName(event, value) {
    this.setState({ name: event.target.value })
  }

  handleChangeType(event, value) {
    this.setState({ name: event.target.value })
  }

  render() {
    return (
      <div>
        <label>Nome do Campo: </label>
        <input
          type="text"
          name={'input' + this.props.id + '_label'}
          value={this.props.name}
          onChange={this.handleChangeName}
        />
        <select
          name={'input' + this.props.id + '_type'}
          value={this.props.type}
          onChange={this.handleChangeType}
        >
          <option value="text">Text</option>
          <option value="date">Date</option>
          <option value="select">Dropdown List</option>
          <option value="checkbox">Multiple Selection</option>
          <option value="file">File</option>
          <option value="image">Image</option>
          <option value="textarea">Long Text</option>
        </select>
      </div>
    )
  }
}

class FormHackathon extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      hackathonName: '',
      fields: [],
    }

    this.handleChange = this.handleChange.bind(this)
  }

  handleChange(event, value) {
    this.setState({ [event.target.hackathonName]: event.target.value })
  }

  addField(field) {
    console.log(field)
    this.setState(prevState => ({
      fields: [
        ...prevState.fields,
        {
          id: field.props.children[0].state.id,
          label: field.props.children[0].state.name,
          type: field.props.children[0].state.type,
        },
      ],
    }))
  }

  removeField() {}

  render() {
    const metadata_fields = this.state.fields
    // Rendering current fields with "remove" button
    let current_metadata_fields = metadata_fields.map((field, index) => {
      return (
        <div key={index}>
          <MetadataField id={index} name={field.name} type={field.type} />
          <Button text="Remove" id={'remove' + index} />
        </div>
      )
    })
    // Rendering empty field with "add" button
    current_metadata_fields.push(
      <div key={current_metadata_fields.length}>
        <MetadataField id={current_metadata_fields.length} />
        <Button
          text="Add"
          id={'add' + current_metadata_fields.length}
          onClick={() =>
            this.addField(
              current_metadata_fields[current_metadata_fields.length - 1]
            )
          }
        />
      </div>
    )
    console.log(this.state.fields)

    return (
      <form>
        <label>
          <h2>DADOS BÁSICOS</h2>
          Nome: &nbsp;
          <input
            type="text"
            name="hackathonName"
            value={this.state.startDateEvent}
            onChange={this.handleChange}
          />
        </label>
        <h2>DADOS DOS PARTICIPANTES</h2>
        {current_metadata_fields}
      </form>
    )
  }
}

export default FormHackathon
